using System.Linq;

using GDServer.Models;
using GDServer.Services;

using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;

using MongoDB.Entities;

using Newtonsoft.Json.Linq;

namespace GDServer.Controllers
{
    [ApiController]
     [Route("api/[controller]/[action]")]
    public class VehicleController : ControllerBase
    {
        private readonly ILogger<VehicleController> _logger;
        private readonly VehiclesService _vehsService;

        public VehicleController(VehiclesService folksService, ILogger<VehicleController> logger)
        {
            _vehsService = folksService;
            _logger = logger;
        }

        [HttpPost]
        public IActionResult Add(LocalVehicle lv)
        {
            if (lv == null) return BadRequest();
            if (string.IsNullOrEmpty(lv.Model)) return BadRequest();
            var dbVehicle = _vehsService.Find(x => x.ModelName == lv.Model && x.OwnerIds.Contains(lv.Owner)).FirstOrDefault();
            if (dbVehicle != null)
            {
                var veh = lv.ToVehicle();
                _vehsService.ReplaceOne(dbVehicle.ID, veh);
            }
            else
            {
                _vehsService.Create(lv.ToVehicle());
            }
            return Ok();
        }

    }
}