import React, { Component } from "react";
import { Route } from "react-router";
import { Layout } from "./components/Layout";

import "./custom.css";
import Landing from "./components/Landing/Landing";
import DataBase from "./components/Database";
import PersonScreen from "./components/Person";
import PluginDownload from "./components/PluginDownload";

export default class App extends Component {
  static displayName = App.name;

  render() {
    return (
      <Layout>
        <Route exact path="/" component={Landing} />
        <Route path="/db" component={DataBase} />
        <Route path="/plugin" component={PluginDownload} />
        {/* <Route path="/db/person" component={PersonScreen}  /> */}
      </Layout>
    );
  }
}
