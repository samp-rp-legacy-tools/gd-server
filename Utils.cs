using System;
using System.Collections.Generic;

using GDServer.Models;

namespace GDServer
{
    public class Utils
    {
        public static Person TransformLocalPerson(LocalPerson lp)
        {

            var p = new Person();
            p.Name = lp.Name;
            p.HomeAddress = lp.HomeAddress;
            p.Married = lp.Married != "Отсутствует";
            if (p.Married) p.MarriedOn = lp.Married;
            else p.MarriedOn = null;
            p.SkinId = lp.SkinId;
            p.OrgPosition = lp.OrgPosition;
            p.City = "Los Santos";
            p.Faction = ParseFaction(lp.Faction);
            return p;
        }
        public static Faction ParseFaction(string faction)
        {
            if (string.IsNullOrEmpty(faction)) return Faction.None;
            faction = faction.Replace(" ", "");
            Faction result = Faction.None;
            if (Enum.TryParse(faction, true, out result))
                Console.WriteLine("Распарсили место работы");
            else
            {
                Console.WriteLine("не получилось распарсить место работы " + faction);
                return Faction.None;

            }
            return result;
        }
    }
    public static class Extensions
    {
        public static Person ToPerson(this LocalPerson lp)
        {
            return Utils.TransformLocalPerson(lp);
        }
        public static Vehicle ToVehicle(this LocalVehicle lv)
        {
            return new Vehicle()
            {
                ModelName = lv.Model,
                    OwnerIds = new List<string>() { lv.Owner }
            };
        }
    }
    public enum Faction
    {
        PoliceLS,
        PoliceSF,
        PoliceLV,
        FBI,

        Ballas,
        Vagos,
        Groove,
        Aztecas,
        Rifa,

        Yakuza,
        RussianMafia,
        LaCostraNorsa,

        Mayor,
        Medic,
        Instructors,

        SFa,
        LVa,
        None,
        NewSF,
        NewsLS,
        NewsLV,

    }
}