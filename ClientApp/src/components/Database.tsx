import * as React from "react";
import { Container, Input, Spinner, Collapse } from "reactstrap";
import "public/css/database.css";
import { Person, Faction } from "./defs";
import debounce from "lodash.debounce";
import { Link, withRouter, RouteComponentProps } from "react-router-dom";
export interface IDataBaseProps extends RouteComponentProps {}

export interface IDataBaseState {
  list: Person[];
  searchStr: string;
  searchingList: Person[];
  colapses: boolean[];
}

class DataBase extends React.Component<IDataBaseProps, IDataBaseState> {
  constructor(props: IDataBaseProps) {
    super(props);
    this.state = {
      list: [],
      searchStr: "",
      searchingList: [],
      colapses: [],
    };
    this.OnSearch = this.OnSearch.bind(this);
    this.ProcessSearch = debounce(this.ProcessSearch, 45);
  }
  async componentDidMount() {
    const db = await fetch("api/person/db").then(r => r.json());
    // console.dir(db);
    this.setState({
      list: db,
      searchingList: db,
      colapses: new Array(db.length).fill(false),
    });
  }
  private ProcessSearch() {
    let input = this.state.searchStr;
    // console.log(input);

    let slist = [];
    if (input != "") slist = this.state.list.filter(x => x.Name.includes(input));
    else slist = this.state.list;
    this.setState({
      searchingList: slist,
    });
  }
  private OnSearch(event: React.ChangeEvent<HTMLInputElement>) {
    this.setState({
      searchStr: event.currentTarget.value,
    });
    this.ProcessSearch();
  }
  private onColshapeClick(i: number) {
    const shapes = this.state.colapses;
    shapes[i] = !shapes[i];
    this.setState({
      colapses: shapes,
    });
  }
  private renderList() {
    return this.state.searchingList.map((x, i) => (
      <div key={i} onClick={() => this.onColshapeClick(i)}>
        <div className="list-line">
          <span>{x.Name}</span>
          <span>{Faction[x.Faction]}</span>
          <span>{x.City || "Неизвестно"}</span>
          <span>{x.Crimes?.length}</span>
        </div>
        <Collapse isOpen={this.state.colapses[i]}>{this.renderPerson(x)}</Collapse>
      </div>
    ));
  }
  private renderPerson(x: Person) {
    return (
      <div className="person-panel">
        <div className="panel-content">
          <h2 className="person-name">{x.Name}</h2>
          <div className="person-fields">
            <span>Фракция: {Faction[x.Faction]}</span>
            <span>Город: {x.City || "Неизвестно"}</span>
            <span>Должность: {x.OrgPosition}</span>
            <span>Преступлений: {x.Crimes?.length}</span>
            <span>Место жительства: {x.HomeAddress}</span>
            <span>Брак: {x.Married ? x.MarriedOn : "Отсутствует"}</span>
          </div>
          <div className="desc">
            {x.Descriptions.map(d => (
              <>
                <h3>{d.CopName}</h3>
                <span>{d.WhenAdded}</span>
                <p>{d.Text}</p>
              </>
            ))}
          </div>
        </div>
        <img className="skin-img" src={"/static/images/skins/Skin" + x.SkinId + ".jpg"} />
      </div>
    );
  }
  public render() {
    return (
      <>
        <Container className="db container">
          <React.Suspense
            fallback={
              <div className="loading-spinner">
                <Spinner
                  style={{
                    width: "3rem",
                    height: "3rem",
                  }}
                />
                {/* <span>Загрузка, подождите...</span> */}
              </div>
            }
          >
            <div className="db-hint">Всего записей: {this.state.list.length}</div>
            <Input className="db searchbar" onChange={this.OnSearch} placeholder={"Введите имя"} />
            <div className="list-wrapper">{this.renderList()}</div>
          </React.Suspense>
        </Container>
      </>
    );
  }
}
export default withRouter(DataBase);
