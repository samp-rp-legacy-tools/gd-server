import React, { Component } from "react";
import { Collapse, Container, Navbar, NavbarBrand, NavbarToggler, NavItem, NavLink } from "reactstrap";
import { Link } from "react-router-dom";
import "public/css/navmenu.css";
export class NavMenu extends Component<any, any> {
  static displayName = NavMenu.name;

  constructor(props: any) {
    super(props);

    this.toggleNavbar = this.toggleNavbar.bind(this);
    this.state = {
      collapsed: true,
    };
  }

  toggleNavbar() {
    this.setState({
      collapsed: !this.state.collapsed,
    });
  }

  render() {
    return (
      <Navbar dark className="navbar-expand-sm navbar-toggleable-sm ng-white border-bottom box-shadow nav-back">
        <Container>
          <NavbarBrand tag={Link} to="/">
            <span className="logo-name">State Database | Legacy</span>
          </NavbarBrand>
          <NavbarToggler onClick={this.toggleNavbar} className="mr-2" />
          <Collapse className="d-sm-inline-flex flex-sm-row" isOpen={!this.state.collapsed} navbar>
            <ul className="navbar-nav flex-grow">
              <NavItem>
                <NavLink tag={Link} className="text" to="/db">
                  База
                </NavLink>
              </NavItem>
              <NavItem>
                <NavLink tag={Link} className="text" to="/plugin">
                  Плагин
                </NavLink>
              </NavItem>
            </ul>
          </Collapse>
        </Container>
      </Navbar>
    );
  }
}
