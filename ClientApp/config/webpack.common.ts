import { TsconfigPathsPlugin } from "tsconfig-paths-webpack-plugin";
import MiniCssExtractPlugin from "mini-css-extract-plugin";
import webpack from "webpack";
import path from "path";
import HtmlWebpackPlugin from "html-webpack-plugin";
import { CleanWebpackPlugin } from "clean-webpack-plugin";
const config: webpack.Configuration = {
  devServer: {
    hot: true,
    port: 3000,
    compress: true,
    contentBase: path.resolve(__dirname, "../", "dist"),
    historyApiFallback: true,
  },

  context: path.resolve(__dirname, "../"),
  entry: {
    main: "./src/index.tsx",
  },
  output: {
    path: path.resolve(__dirname, "../", "dist"),
    filename: "[name].js",
  },
  module: {
    rules: [
      {
        exclude: /node_modules/,
        test: /\.ts(x?)$/,
        use: [
          {
            loader: "ts-loader?configFile=../tsconfig.json",
            options: {
              // transpileOnly: true
            },
          },
        ],
      },
      {
        test: /\.html$/,
        loader: "html-loader",
        exclude: /node_modules/,
      },
      {
        test: /\.css$/,
        use: [
          {
            loader: MiniCssExtractPlugin.loader,
            options: {
              esModule: true,
            },
          },
          {
            loader: "css-loader",
          },
        ],
      },
      {
        test: /\.(png|svg|jpg|gif|woff(2)?|ttf|eot|otf)$/,
        use: [
          {
            loader: "file-loader",
            options: {
              esModule: false,
            },
          },
        ],
      },
    ],
  },
  plugins: [
    new CleanWebpackPlugin({
      cleanStaleWebpackAssets: false,
    }),
    new HtmlWebpackPlugin({
      template: "./public/html/index.html",
      filename: "index.html",
      favicon: "./public/favicon.ico",
      chunks: ["main", "vendor"],
    }),
    new MiniCssExtractPlugin({
      filename: "[name].css",
      chunkFilename: "[id].css",
    }),
  ],
  resolve: {
    extensions: [".ts", ".tsx", ".js"],
    plugins: [new TsconfigPathsPlugin({})],
  },

  optimization: {
    splitChunks: {
      name: "vendor",
      chunks: "initial",
    },
  },
};
export default config;
