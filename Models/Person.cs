using System;
using System.Collections.Generic;

using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;
namespace GDServer.Models
{
    public class Person : IDBEntity
    {
        public string Name { get; set; }
        public bool Married { get; set; }
        public string MarriedOn { get; set; }
        public string HomeAddress { get; set; }
        public Faction Faction { get; set; }
        public List<ObjectId> Vehicle { get; set; }
                public string OrgPosition { get; set; }

        public int SkinId { get; set; }
        public string City { get; set; }
        public List<Crime> Crimes { get; set; } = new List<Crime>();
        public List<Description> Descriptions { get; set; } = new List<Description>();
        public override string ToString()
        {
            return $"{this.ID}{Environment.NewLine}{this.Name} {this.Faction.ToString()}{Environment.NewLine} {this.HomeAddress}";
        }
    }
    public class Description
    {
        public string CopName { get; set; }
        public DateTime WhenAdded { get; set; }
        public string Text { get; set; }
    }
    public abstract class Crime
    {
        public DateTime Date { get; set; }
        public int WantedLevel { get; set; }
    }
    public class Robbery : Crime
    {
        public string Target { get; set; }
    }
    public class LocalPerson
    {
        public string Name { get; set; }
        public string Faction { get; set; }
        public int Years { get; set; }
        public string OrgPosition { get; set; }
        public string Married { get; set; }
        public string HomeAddress { get; set; }
        public int SkinId { get; set; }

    }
}