import * as React from "react";
import "public/css/landing.css";
import ReactRotatingText from "react-rotating-text";

import {
  Container,
  FormGroup,
  Label,
  Input,
  Nav,
  NavItem,
  NavLink,
  TabContent,
  TabPane,
  Row,
  Col,
  Card,
  CardTitle,
  CardText,
  Button,
} from "reactstrap";
export interface ILandingProps {}

export interface ILandingState {
  activeTabId: number;
}

export default class Landing extends React.Component<ILandingProps, ILandingState> {
  constructor(props: ILandingProps) {
    super(props);
    this.state = {
      activeTabId: 0,
    };
    document.addEventListener("scroll", ev => {});
  }
  private switchAuthTab(tabId: number) {
    this.setState({
      activeTabId: tabId,
    });
  }
  public render() {
    return (
      <div className="gradient1">
        <div className="jumbo">
          <div className="jumbo-container">
            <div className="jumo-left-content">
              <h1 className="display-4 jumbo-header">State Database</h1>
              <h2>
              </h2>
            </div>
            <div className="jumbo-panel shadow">
              <div>
                <Nav tabs>
                  <NavItem>
                    <NavLink
                      className={(this.state.activeTabId === 0 ? "active" : "") + " acrylic"}
                      onClick={() => this.switchAuthTab(0)}
                    >
                      Вход
                    </NavLink>
                  </NavItem>
                  <NavItem>
                    <NavLink
                      className={(this.state.activeTabId === 1 ? "active" : "") + " acrylic"}
                      onClick={() => this.switchAuthTab(1)}
                    >
                      Регистрация
                    </NavLink>
                  </NavItem>
                </Nav>
                <div className="jumbo-panel-content">
                  <TabContent activeTab={this.state.activeTabId}>
                    <TabPane tabId={0}>
                      <FormGroup>
                        <Label for="exampleEmail" hidden>
                          Email
                        </Label>
                        <Input type="email" name="email" id="exampleEmail" placeholder="Email" />
                      </FormGroup>
                      <FormGroup>
                        <Label for="examplePassword" hidden>
                          Password
                        </Label>
                        <Input type="password" name="password" id="examplePassword" placeholder="Пароль" />
                      </FormGroup>
                      <Button>Вход</Button>
                    </TabPane>
                    <TabPane tabId={1}>
                      <Row>
                        <Col sm="6">
                          <Card body>
                            <CardTitle>Special Title Treatment</CardTitle>
                            <CardText>With supporting text below as a natural lead-in to additional content.</CardText>
                            <Button>Go somewhere</Button>
                          </Card>
                        </Col>
                        <Col sm="6">
                          <Card body>
                            <CardTitle>Special Title Treatment</CardTitle>
                            <CardText>With supporting text below as a natural lead-in to additional content.</CardText>
                            <Button>Go somewhere</Button>
                          </Card>
                        </Col>
                      </Row>
                    </TabPane>
                  </TabContent>
                </div>
              </div>
            </div>
          </div>
        </div>

        {/* <Container fluid>
          <div className="waterfall">
            <h2 className="section-headline">Цели</h2>

            <div className="cards">
              <div className="card-container full">
                <div className="card-content">fragment1</div>
              </div>
              <div className="card-container">
                <div className="card-content">fragment2</div>
              </div>
              <div className="card-container">
                <div className="card-content">fragment3</div>
              </div>
            </div>
          </div>
        </Container>
        <div className="StyledEveryDevice">
          <div className="Inner">
            <div className="Title lclluc visible">
              <h2 className="Heading dtNxrw">
                Natural on
                <br />
                every device
              </h2>
            </div>
            <div className="Content">
              <div className="ContentBlock">
                <p>
                  People move and shift across devices and platforms throughout their day. From laptops to phones, from
                  PCs to holograms. From office to living room, from real to virtual. Experiences seamlessly move and
                  shift with them.
                </p>
              </div>
            </div>
          </div>
        </div> */}
      </div>
    );
  }
}
