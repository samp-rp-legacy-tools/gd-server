using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;

using GDServer.Models;

using MongoDB.Bson;
using MongoDB.Bson.Serialization;
using MongoDB.Bson.Serialization.IdGenerators;
using MongoDB.Driver;

namespace GDServer.Services
{
    public class FolksService
    {
        private readonly IMongoCollection<Person> _folks;

        public FolksService(IGDDatabaseSettings settings)
        {

            var client = new MongoClient("mongodb://localhost:27017");
            var database = client.GetDatabase("gd");

            _folks = database.GetCollection<Person>("folks", new MongoCollectionSettings(){
                AssignIdOnInsert = false
            });
        }

        public List<Person> Get() =>
            _folks.Find(folk => true).ToList();

        public Person GetByName(string name)
        {
            return _folks.Find(x => x.Name == name).FirstOrDefault();
        }
        public Person Get(string ID) =>
            _folks.Find<Person>(folk => folk.ID == ID).FirstOrDefault();

        public Person Create(Person folk)
        {
            _folks.InsertOne(folk);
            return folk;
        }

        public void ReplaceOne(string name, Person person) =>
            _folks.ReplaceOne(folk => folk.Name == name, person, new ReplaceOptions()
            {
                IsUpsert = true
            });

        public void Remove(Person bookIn) =>
            _folks.DeleteOne(folk => folk.ID == bookIn.ID);

        public void Remove(string ID) =>
            _folks.DeleteOne(folk => folk.ID == ID);
        public IEnumerable<Person> Find(Expression<Func<Person, bool>> filter)
        {
            return _folks.Find(filter).ToEnumerable();
        }
    }
}