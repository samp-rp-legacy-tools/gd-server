namespace GDServer.Game
{
    public enum VehicleCategory
    {
        Airplane = 1,
        Helicopter = 2,
        Bike = 3,
        Convertible = 4,
        Industrial = 5,
        Lowrider = 6,

        OffRoad = 7,

        PublicService = 8,

        Saloon = 9,

        Sport = 10,

        Station = 11,

        Boat = 12,

        Trailer = 13,

        Unique = 14,
        RemoteControl = 15,

        TrainTrailer = 16
    }
}