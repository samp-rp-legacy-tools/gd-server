namespace GDServer.Models
{
    public class GDDatabaseSettings : IGDDatabaseSettings
    {
        public string FolksCollectionName { get; set; }
        public string VehiclesCollectionName { get; set; }

        public string ConnectionString { get; set; }
        public string DatabaseName { get; set; }
    }

    public interface IGDDatabaseSettings
    {
        string FolksCollectionName { get; set; }
        string VehiclesCollectionName { get; set; }

        string ConnectionString { get; set; }
        string DatabaseName { get; set; }
    }
}