import "public/css/bootstrap.min.css";
import React from "react";
import ReactDOM from "react-dom";
import { Router } from "react-router-dom";
import App from "./App";
import { createBrowserHistory } from "history";

const baseUrl = "/";
let history = createBrowserHistory()
ReactDOM.render(
  <Router history={history}>
    <App />
  </Router>,
  document.getElementById("app")
);
