using System;
using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;

public abstract class IDBEntity
{
    public DateTime LastTimeUpdated { get; set; }

    [BsonId]
    [BsonIgnoreIfDefault]
    [BsonRepresentation(BsonType.ObjectId)]
    public string ID { get; set; }
    public IDBEntity()
    {
        // ID = Guid.NewGuid().ToString();
        LastTimeUpdated = DateTime.Now;
    }
}