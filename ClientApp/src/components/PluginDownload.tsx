import * as React from "react";
import { Container, Card, CardHeader, CardBody, Alert, CardText, Button } from "reactstrap";
import "public/css/plugin.css";
export interface IPluginDownloadProps {}

export interface IPluginDownloadState {}

export default class PluginDownload extends React.Component<IPluginDownloadProps, IPluginDownloadState> {
  constructor(props: IPluginDownloadProps) {
    super(props);

    this.state = {};
  }

  public render() {
    return (
      <Container className="p container">
        <Alert color="danger">
          Внимание! Плагин находится в стадии активной разработки, поэтому большинство функций недоступно или работают
          некорректно
        </Alert>
        <Card>
          <CardHeader>Установка</CardHeader>
          <CardBody>
            <CardText>
              <p>Требования для работы плагина: </p>
              <ul>
                <li>
                  <code>Moonloader 0.26+</code>
                </li>
                <li>
                  <code>SAMPFUNCS</code>
                </li>
              </ul>
              <p>
                Распаковать содержимое архива в папку <code><samp>moonloader</samp></code> в директории игры
              </p>
              <p>
                После зайти в игру (либо, если уже находитесь в ней, нажать{" "}
                <kbd>
                  <kbd>CTRL</kbd> + <kbd>R</kbd>
                </kbd>
                )
              </p>
              <p>
                Включить интерфейс - <kbd>F2</kbd>
              </p>
              <Button
                onClick={() => {
                  window.open("/static/lua/gd_foxpro.zip");
                }}
              >
                Скачать плагин
              </Button>
              <p
                style={{
                  paddingTop: 10,
                }}
              >
                Исходный код можно найти здесь: <code>https://gitlab.com/samp-rp-legacy-tools</code>
              </p>
            </CardText>
          </CardBody>
        </Card>
      </Container>
    );
  }
}
