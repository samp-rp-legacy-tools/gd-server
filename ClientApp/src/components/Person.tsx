import * as React from "react";
import { RouteComponentProps, withRouter } from "react-router-dom";
import { Person } from "./defs";

export interface IPersonScreenProps extends RouteComponentProps {}

export interface IPersonScreenState {}

class PersonScreen extends React.Component<IPersonScreenProps, IPersonScreenState> {
  constructor(props: IPersonScreenProps) {
    super(props);

    this.state = {};
  }

  public render() {
    return <div>{this.props.location.state.person.Name}</div>;
  }
}
export default withRouter(PersonScreen);
