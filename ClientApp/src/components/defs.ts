export enum Faction {
  PoliceLS,
  PoliceSF,
  PoliceLV,
  FBI,

  Ballas,
  Vagos,
  Groove,
  Aztecas,
  Rifa,

  Yakuza,
  RussianMafia,
  LaCostraNorsa,

  Mayor,
  Medic,
  Instructors,

  SFa,
  LVa,
  None,
}
export interface Person {
  Name: string;
  Married: boolean;
  MarriedOn: string;
  HomeAddress: string;
  Faction: Faction;
  Vehicle: string[];
  SkinId: number;
  City: string;
  Crimes: Crime[];
  Descriptions: Description[];
  OrgPosition: string;
}
export interface Description {
  CopName: string;
  WhenAdded: Date | string;
  Text: string;
}
export interface Crime {
  Date: Date | string;
  WantedLevel: number;
}
export interface Robbery extends Crime {
  Target: string;
}
