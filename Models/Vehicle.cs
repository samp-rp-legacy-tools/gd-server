using System.Collections.Generic;

using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;
namespace GDServer.Models
{
    public class Vehicle : IDBEntity
    {
        public string Color { get; set; }
        public string ModelName { get; set; }
        public List<string> OwnerIds { get; set; } = new List<string>();
    }
    public class LocalVehicle
    {
        public string Model { get; set; }
        public string Owner { get; set; }
        public int Engine { get; set; }
    }
}