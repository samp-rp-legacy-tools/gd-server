using System.Collections.Generic;

using GDServer.Models;
using GDServer.Services;

using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;

namespace GDServer.Controllers
{
    [ApiController]
    [Route("api/[controller]/[action]")]
    public class PersonController : ControllerBase
    {
        private readonly ILogger<PersonController> _logger;
        private readonly FolksService _folksService;

        public PersonController(FolksService folksService, ILogger<PersonController> logger)
        {
            _folksService = folksService;
            _logger = logger;
        }

        [HttpGet]
        public Person GetPersonByName(string name)
        {
            return new Person()
            {
                Name = name
            };
        }

        [HttpGet]
        public IEnumerable<Person> Db()
        {
            return _folksService.Get();
        }

        [HttpPost]
        public IActionResult Auth(LocalPerson person)
        {
            if (person == null) return BadRequest();
            if (string.IsNullOrEmpty(person.Name)) return BadRequest();
            var dbPerson = _folksService.GetByName(person.Name);
            _folksService.ReplaceOne(person.Name, person.ToPerson());
            return Ok();
        }

        [HttpPost]
        public IActionResult Add(LocalPerson p)
        {
            if (p == null) return BadRequest();
            if (string.IsNullOrEmpty(p.Name)) return BadRequest();
            var dbPerson = _folksService.GetByName(p.Name);
            var person = p.ToPerson();
            _logger.LogInformation("Новый персонаж создан");
            _logger.LogInformation(person.ToString());
            _folksService.ReplaceOne(p.Name, person);
            return Ok();
        }

        [HttpPost]
        public IActionResult AddIfNotExist(Person person)
        {
            return Ok();
        }
    }
}