using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;

using GDServer.Models;

using MongoDB.Driver;

namespace GDServer.Services
{
    public class VehiclesService
    {
        private readonly IMongoCollection<Vehicle> _vehicles;

        public VehiclesService(IGDDatabaseSettings settings)
        {
            var client = new MongoClient("mongodb://localhost:27017");
            var database = client.GetDatabase("gd");

            _vehicles = database.GetCollection<Vehicle>("vehicles");
        }

        public List<Vehicle> Get() =>
            _vehicles.Find(veh => true).ToList();

        public Vehicle Get(string id) =>
            _vehicles.Find<Vehicle>(veh => veh.ID == id).FirstOrDefault();

        public Vehicle Create(Vehicle veh)
        {
            _vehicles.InsertOne(veh);
            return veh;
        }

        public void Update(string modelName, string owner, Vehicle veh) =>
            _vehicles.ReplaceOne(veh => veh.ModelName == modelName && veh.OwnerIds.Contains(owner), veh);

        public void Remove(Vehicle bookIn) =>
            _vehicles.DeleteOne(veh => veh.ID == bookIn.ID);

        public void Remove(string id) =>
            _vehicles.DeleteOne(veh => veh.ID == id);

        public void ReplaceOne(string id, Vehicle veh) =>
            _vehicles.ReplaceOne(folk => folk.ID == id, veh, new ReplaceOptions()
            {
                IsUpsert = true
            });

        public IEnumerable<Vehicle> Find(Expression<Func<Vehicle, bool>> filter)
        {
            return _vehicles.Find(filter).ToEnumerable();
        }
    }
}