using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;

namespace GDServer.Controllers
{
    [ApiController]
    [Route("api/[controller]/[action]")]
    public class MiscController : ControllerBase
    {
        private readonly ILogger<MiscController> _logger;

        public MiscController(ILogger<MiscController> logger)
        {
            _logger = logger;
        }
        
        [HttpGet, ActionName("Version")]
        public ActionResult<string> Version()
        {
            return "0.3";
        }

        [HttpGet]
        public string Author()
        {
            return "FoxPro";
        }
    }
}